﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;

namespace Scrubs
{
    public partial class ShowPatient : Form
    {
        public ShowPatient()
        {
            InitializeComponent();
            comboBox1.SelectedIndex = 1;
        }

        private void ShowPatient_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "scrubsDataSet.Patient". При необходимости она может быть перемещена или удалена.
            this.patientTableAdapter.Fill(this.scrubsDataSet.Patient);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" && textBox2.Text == "" && textBox3.Text == "")
            {
                MessageBox.Show("Все поля пусты!", "Сообщение", MessageBoxButtons.OK);
            }
            else
            { 
                DialogResult rez = MessageBox.Show("Вы уверенны, что хотите обновить данные?","Обновление",MessageBoxButtons.OKCancel,MessageBoxIcon.Warning);

                if (rez == DialogResult.OK)
                {
                    try
                    {
                        patientTableAdapter.Update(this.scrubsDataSet);
                        MessageBox.Show("Изменения сохраненны успешно", "OK", MessageBoxButtons.OK);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(),"Ошибка", MessageBoxButtons.OK);
                    }
                }
             }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            string Field = "FIO";

            switch (comboBox1.SelectedIndex)
            {
                case 0: { Field = "id"; break; }
                case 1: { Field = "FIO"; break; }
                case 2: { Field = "Adress"; break; }
                case 3: { Field = "Phone"; break; }
            }

            string selectString = Field + " Like '%" + textBox4.Text.Trim() + "%'";

            DataRowCollection allRows = (scrubsDataSet.Patient).Rows;
            
            if (Field != "id")
            {
                DataRow[] searchedRows = (scrubsDataSet.Patient).Select(selectString);
                
                if (searchedRows.Count() != 0)
                {
                    int rowIndex = allRows.IndexOf(searchedRows[0]);

                    dataGridView1.CurrentCell = dataGridView1[0, rowIndex];
                }
            }
            else 
            {
                if (textBox4.Text != "")
                { 
                    DataRow searchedRows = (scrubsDataSet.Patient).FindByid(Int32.Parse(textBox4.Text.Trim()));

                    if (searchedRows != null)
                    { 
                        int rowIndex = allRows.IndexOf(searchedRows);

                        dataGridView1.CurrentCell = dataGridView1[0, rowIndex];
                    }
                }
            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" && textBox2.Text == "" && textBox3.Text == "")
            {
                MessageBox.Show("Все поля пусты!", "Сообщение", MessageBoxButtons.OK);
            }
            else
            {
                try
                {
                    SqlConnectionStringBuilder bldr = new SqlConnectionStringBuilder();
                    bldr.DataSource = @"sowa111111-пк\sqlexpress";
                    bldr.InitialCatalog = "Scrubs";
                    bldr.IntegratedSecurity = true;
                    SqlConnection cn = new SqlConnection(bldr.ConnectionString);

                    cn.Open();

                    string strSQl = "select count(*) from Patient";
                    SqlCommand cmd = new SqlCommand(strSQl, cn);
                    int countP = Convert.ToInt32(cmd.ExecuteScalar());

                    cn.Close();

                    patientTableAdapter.Insert(countP+1, textBox1.Text, textBox2.Text, textBox3.Text);
                    this.ShowPatient_Load(sender, e);
                    MessageBox.Show("Данные добавленны!", "Сообщение", MessageBoxButtons.OK);
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), "Сообщение", MessageBoxButtons.OK);
                }

            }
        }

        private void clearFields_Click(object sender, EventArgs e)
        {
            this.textBox1.Clear();
            this.textBox2.Clear();
            this.textBox3.Clear();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

      
    }
}
