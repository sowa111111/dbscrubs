﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Scrubs
{
    public partial class Visits : Form
    {
        public Visits()
        {
            InitializeComponent();
        }

        private void Visits_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "scrubsDataSet4.Doctor". При необходимости она может быть перемещена или удалена.
            this.doctorTableAdapter.Fill(this.scrubsDataSet4.Doctor);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "scrubsDataSet4.Patient". При необходимости она может быть перемещена или удалена.
            this.patientTableAdapter.Fill(this.scrubsDataSet4.Patient);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "scrubsDataSet4.Visits". При необходимости она может быть перемещена или удалена.
            this.visitsTableAdapter.Fill(this.scrubsDataSet4.Visits);

        }

        private void exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void clearFields_Click(object sender, EventArgs e)
        {
            this.historyField.Clear();
            this.treatmentField.Clear();
            this.diagnosisField.Clear();
            this.costField.Clear();
            this.costMetField.Clear();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (diagnosisField.Text.Trim() == "" && historyField.Text.Trim() == "" && treatmentField.Text.Trim() == "" && costField.Text.Trim() == "" && costMetField.Text.Trim() == "")
            {
                MessageBox.Show("Все поля пусты!", "Сообщение", MessageBoxButtons.OK);
            }
            else
            {
                DialogResult rez = MessageBox.Show("Вы уверенны, что хотите обновить данные?", "Обновление", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);

                if (rez == DialogResult.OK)
                {
                    try
                    {
                        visitsTableAdapter.Update(scrubsDataSet4.Visits);
                        MessageBox.Show("Изменения сохраненны успешно", "OK", MessageBoxButtons.OK);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Ошибка", MessageBoxButtons.OK);
                    }
                }
            }
        }

        private void addVisit_Click(object sender, EventArgs e)
        {
            if (diagnosisField.Text.Trim() == "" && historyField.Text.Trim() == "" && treatmentField.Text.Trim() == "" && costField.Text.Trim() == "" && costMetField.Text.Trim() == "")
            {
                MessageBox.Show("Все поля пусты!", "Сообщение", MessageBoxButtons.OK);
            }
            else
            {
                try
                {
                    SqlConnectionStringBuilder bldr = new SqlConnectionStringBuilder();
                    bldr.DataSource = @"sowa111111-пк\sqlexpress";
                    bldr.InitialCatalog = "Scrubs";
                    bldr.IntegratedSecurity = true;
                    SqlConnection cn = new SqlConnection(bldr.ConnectionString);

                    cn.Open();

                    string strSQl = "select count(*) from Visits";
                    SqlCommand cmd = new SqlCommand(strSQl, cn);
                    int countP = Convert.ToInt32(cmd.ExecuteScalar());

                    cn.Close();

                    int id_speciality = int.Parse(doctorComboBox.SelectedValue.ToString());
                    int id_patient = int.Parse(patientComboBox.SelectedValue.ToString());
                    DateTime dateV = dateTimePicker1.Value;
                    int cost = int.Parse(costField.Text.ToString().Trim());
                    int costM = int.Parse(costMetField.Text.ToString().Trim());

                    visitsTableAdapter.Insert(countP + 1, id_patient, id_speciality, dateV, historyField.Text, diagnosisField.Text, treatmentField.Text, cost,costM);
                    
                    this.Visits_Load(sender, e);
                    MessageBox.Show("Данные добавленны!", "Сообщение", MessageBoxButtons.OK);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), "Сообщение", MessageBoxButtons.OK);
                }

            }

        }
    }
}
