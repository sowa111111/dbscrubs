﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Scrubs
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void новыйПациентToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void просмотрToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowPatient ob = new ShowPatient();
            ob.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult rez = MessageBox.Show("Хотите выйти из прмложения?", "Окно выхода", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (rez == DialogResult.OK)
            {
                Application.Exit();
            }
        }

        private void SpecialityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Speciality ob = new Speciality();
            ob.ShowDialog();
        }

        private void doctorsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Doctors ob = new Doctors();
            ob.ShowDialog();
        }

        private void visitsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Visits ob = new Visits();
            ob.ShowDialog();
        }

        private void allPatientVisitsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PatientVisits ob = new PatientVisits();
            ob.ShowDialog();
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About ob = new About();
            ob.ShowDialog();
        }
    }
}
