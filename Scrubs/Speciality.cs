﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Scrubs
{
    public partial class Speciality : Form
    {
        public Speciality()
        {
            InitializeComponent();
        }

        private void Speciality_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "scrubsDataSet1.Speciality". При необходимости она может быть перемещена или удалена.
            this.specialityTableAdapter.Fill(this.scrubsDataSet1.Speciality);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void clearFields_Click(object sender, EventArgs e)
        {
            this.specialityField.Clear();
        }

        private void saveChange_Click(object sender, EventArgs e)
        {
            if (specialityField.Text.Trim() == "")
            {
                MessageBox.Show("Поле пусто!", "Сообщение", MessageBoxButtons.OK);
            }
            else
            {
                DialogResult rez = MessageBox.Show("Вы уверенны, что хотите обновить данные?", "Обновление", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);

                if (rez == DialogResult.OK)
                {
                    try
                    {
                        specialityTableAdapter.Update(this.scrubsDataSet1);
                        MessageBox.Show("Изменения сохраненны успешно", "OK", MessageBoxButtons.OK);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Ошибка", MessageBoxButtons.OK);
                    }
                }
            }
        }

        private void addSpeciality_Click(object sender, EventArgs e)
        {
            if (this.specialityField.Text.Trim() == "")
            {
                MessageBox.Show("Заполните поле специализации!", "Сообщение", MessageBoxButtons.OK);
            }
            else
            {
                try
                {
                    SqlConnectionStringBuilder bldr = new SqlConnectionStringBuilder();
                    bldr.DataSource = @"sowa111111-пк\sqlexpress";
                    bldr.InitialCatalog = "Scrubs";
                    bldr.IntegratedSecurity = true;
                    SqlConnection cn = new SqlConnection(bldr.ConnectionString);

                    cn.Open();

                    string strSQl = "select count(*) from Speciality";
                    SqlCommand cmd = new SqlCommand(strSQl, cn);
                    int countP = Convert.ToInt32(cmd.ExecuteScalar());

                    cn.Close();

                    specialityTableAdapter.Insert(countP + 1, specialityField.Text);
                    this.Speciality_Load(sender, e);
                    MessageBox.Show("Данные добавленны!", "Сообщение", MessageBoxButtons.OK);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), "Сообщение", MessageBoxButtons.OK);
                }

            }
        }

        private void searchBox_TextChanged(object sender, EventArgs e)
        {
            string selectString = "name Like '%" + searchBox.Text.Trim() + "%'";

            DataRowCollection allRows = (scrubsDataSet1.Speciality).Rows;

            DataRow[] searchedRows = (scrubsDataSet1.Speciality).Select(selectString);

            if (searchedRows.Count() != 0)
            {
                int rowIndex = allRows.IndexOf(searchedRows[0]);

                dataGridView1.CurrentCell = dataGridView1[0, rowIndex];
            }
           
        }
    }
}
