﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Scrubs
{
    public partial class Doctors : Form
    {
        public Doctors()
        {
            InitializeComponent();
        }

        private void Doctors_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "scrubsDataSet4.Speciality". При необходимости она может быть перемещена или удалена.
            this.specialityTableAdapter.Fill(this.scrubsDataSet4.Speciality);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "scrubsDataSet4.Doctor". При необходимости она может быть перемещена или удалена.
            this.doctorTableAdapter.Fill(this.scrubsDataSet4.Doctor);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "scrubsDataSet2.Doctor". При необходимости она может быть перемещена или удалена.

            

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void clearFields_Click(object sender, EventArgs e)
        {
            this.fioField.Clear();
            this.adresField.Clear();
            this.phoneField.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (fioField.Text.Trim() == "" && adresField.Text.Trim() == "" && phoneField.Text.Trim() == "")
            {
                MessageBox.Show("Все поля пусты!", "Сообщение", MessageBoxButtons.OK);
            }
            else
            {
                DialogResult rez = MessageBox.Show("Вы уверенны, что хотите обновить данные?", "Обновление", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);

                if (rez == DialogResult.OK)
                {
                    try
                    {
                        doctorTableAdapter.Update(scrubsDataSet4.Doctor);
                        MessageBox.Show("Изменения сохраненны успешно", "OK", MessageBoxButtons.OK);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Ошибка", MessageBoxButtons.OK);
                    }
                }
            }
        }

        private void addDoctor_Click(object sender, EventArgs e)
        {
            if (fioField.Text.Trim() == "" && adresField.Text.Trim() == "" && phoneField.Text.Trim() == "")
            {
                MessageBox.Show("Все поля пусты!", "Сообщение", MessageBoxButtons.OK);
            }
            else
            {
                try
                {
                    SqlConnectionStringBuilder bldr = new SqlConnectionStringBuilder();
                    bldr.DataSource = @"sowa111111-пк\sqlexpress";
                    bldr.InitialCatalog = "Scrubs";
                    bldr.IntegratedSecurity = true;
                    SqlConnection cn = new SqlConnection(bldr.ConnectionString);

                    cn.Open();

                    string strSQl = "select count(*) from Doctor";
                    SqlCommand cmd = new SqlCommand(strSQl, cn);
                    int countP = Convert.ToInt32(cmd.ExecuteScalar());

                    cn.Close();

                    int id_speciality = int.Parse(specialityComboBox.SelectedValue.ToString());

                    
                    doctorTableAdapter.Insert(countP + 1, fioField.Text,id_speciality, adresField.Text, phoneField.Text);
                    this.Doctors_Load(sender, e);
                    MessageBox.Show("Данные добавленны!", "Сообщение", MessageBoxButtons.OK);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), "Сообщение", MessageBoxButtons.OK);
                }

            }
        }

        private void search_TextChanged(object sender, EventArgs e)
        {
            string Field = "FIO";

            switch (searchComboBox.SelectedIndex)
            {
                case 0: { Field = "id"; break; }
                case 1: { Field = "FIO"; break; }
                case 2: { Field = "adress"; break; }
                case 3: { Field = "phone"; break; }
            }

            string selectString = Field + " Like '%" + search.Text.Trim() + "%'";

            DataRowCollection allRows = (scrubsDataSet4.Doctor).Rows;

            if (Field != "id" )
            {
                DataRow[] searchedRows = (scrubsDataSet4.Doctor).Select(selectString);

                if (searchedRows.Count() != 0)
                {
                    int rowIndex = allRows.IndexOf(searchedRows[0]);

                    dataGridView1.CurrentCell = dataGridView1[0, rowIndex];
                }
            }
            else
            {
                if (search.Text != "")
                {
                    DataRow searchedRows = (scrubsDataSet4.Doctor).FindByid(Int32.Parse(search.Text.Trim()));
                    
                    if (searchedRows != null)
                    {
                        int rowIndex = allRows.IndexOf(searchedRows);

                        dataGridView1.CurrentCell = dataGridView1[0, rowIndex];
                    }
                }
            }

        }

        
    }
}
